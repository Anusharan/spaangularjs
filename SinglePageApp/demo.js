﻿/// <reference path="scripts/angular.js" />

var MyApp = angular.module("MyApp", ['ngRoute','EmployeeService']);

MyApp.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider.
            when('/Add', {
                templateUrl: 'Views/add.html',
                controller: 'AddController'
            }).
            when('/Edit', {
                templateUrl: 'Views/edit.html',
                controller: 'EditController'
            }).
            when('/Delete', {
                templateUrl: 'Views/delete.html',
                controller: 'DeleteController'
            }).
            when('/Home', {
                templateUrl: 'Views/home.html',
                controller: 'HomeController'
            }).
            otherwise({
                redirectTo: '/Home'
            });
    }]);

MyApp.controller("AddController", function ($scope, EmpApi) {

    $scope.addEmp = function () {
        var empToAdd = {
            'Name' : $scope.name,
            'City': $scope.city
        };
        EmpApi.AddEmployee(empToAdd)
            .then(function (resonse) {
                alert("User Added!");
                $scope.name = undefined;
                $scope.city = undefined;
            }), (function (error) {
                $scope.status = 'Error while adding employee!';
            })
    }
});

MyApp.controller("EditController", function ($scope) {
    $scope.message = "In Edit view";
});

MyApp.controller("DeleteController", function ($scope) {
    $scope.message = "In Delete view";
});

MyApp.controller("HomeController", function ($scope, EmpApi) {
    getEmployees();
    function getEmployees() {
        EmpApi.getEmployees().then(function (response) {
            $scope.emps = response.data;
        }),(function (error) {
                $scope.status = 'Unable to load emp data: ' + error.message;
            })
    }
});
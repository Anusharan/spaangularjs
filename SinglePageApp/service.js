﻿/// <reference path="scripts/angular.js" />


var EmployeeService = angular.module('EmployeeService', []);
EmployeeService.factory('EmpApi', function ($http) {
    var urlBase = "http://localhost:14794/api";
    var EmpApi = {};

    EmpApi.AddEmployee = function (emp) {
        console.log(emp);
        var req = $http({
            method: 'post',
            url: urlBase + '/tblEmployees/',
            data: emp
        });
        return req;
        //return $http.post(urlBase + '/tblEmployees/' + emp);
    };

    EmpApi.getEmployees = function () {
        return $http.get(urlBase + '/tblEmployees');
    };

       EmpApi.EditEmployee = function (empToUpdate) {
        var request = $http({
            method: 'put',
            url: urlBase + '/tblEmployees/' + empToUpdate.Id,
            data: empToUpdate
        });
           return request;
        //return $http.post(urlBase + '/tblEmployees/' + emp);
    };

    return EmpApi;
});